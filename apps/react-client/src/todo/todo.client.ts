import type { AsyncThunkPayloadCreator } from '@reduxjs/toolkit';
import { $fetch } from 'ohmyfetch';

import { CreateTodo, Todo, UpdateTodo } from 'todo/todo.interface';

const client = $fetch.create({ baseURL: process.env['REACT_APP_API_URL'] });

export const createTodo: AsyncThunkPayloadCreator<Todo, CreateTodo> = async (
  newTodo,
) => {
  return client<Todo>('/todo', {
    method: 'POST',
    body: newTodo,
  });
};

export const listTodos: AsyncThunkPayloadCreator<Todo[]> = async (
  _,
  thunkApi,
) => {
  return client<Todo[]>('/todo', { signal: thunkApi.signal });
};

export const updateTodo: AsyncThunkPayloadCreator<
  Todo,
  UpdateTodo & Pick<Todo, 'id'>
> = async ({ id, ...changes }) => {
  return client<Todo>(`/todo/${id}`, {
    method: 'PATCH',
    body: changes,
  });
};

export const toggleTodos: AsyncThunkPayloadCreator<Todo[], boolean> = async (
  completed,
) => {
  return client<Todo[]>('/todo', {
    method: 'PATCH',
    body: { completed },
    redirect: 'follow',
  });
};

export const deleteTodo: AsyncThunkPayloadCreator<void, Todo['id']> = async (
  id,
) => {
  await client(`/todo/${id}`, { method: 'DELETE' });
};

export const removeTodos: AsyncThunkPayloadCreator<void> = async () => {
  await client('/todo', { method: 'DELETE' });
};
