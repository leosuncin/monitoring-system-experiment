import { HttpStatus, type INestApplication } from '@nestjs/common';
import request from 'supertest';

import { bootstrap } from '~app/main';

describe('HealthController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    app = await bootstrap({ logger: false });
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be healthy', async () => {
    await request(app.getHttpServer())
      .get('/health')
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(body).toMatchObject({
          details: {
            database: {
              status: 'up',
            },
            mem_heap: {
              status: 'up',
            },
            mem_rss: {
              status: 'up',
            },
          },
          error: {},
          info: {
            database: {
              status: 'up',
            },
            mem_heap: {
              status: 'up',
            },
            mem_rss: {
              status: 'up',
            },
          },
          status: 'ok',
        });
      });
  });
});
