import { faker } from '@faker-js/faker';
import { HttpStatus, type INestApplication } from '@nestjs/common';
import { encode } from 'node:querystring';
import request from 'supertest';

import { bootstrap } from '~app/main';
import type { Todo } from '~app/todo/entities/todo.entity';
import { TodoService } from '~app/todo/services/todo.service';

describe('TodoController (E2E)', () => {
  let app: INestApplication;
  let todo: Todo;

  beforeAll(async () => {
    app = await bootstrap({ logger: false });
    await app.init();
  });

  beforeEach(async () => {
    const todoService = app.get(TodoService);
    todo = await todoService.create({ title: faker.lorem.sentence() });
  });

  afterAll(async () => {
    await app.close();
  });

  it.each([
    {
      title: faker.hacker.phrase(),
    },
    {
      title: faker.hacker.phrase(),
      order: faker.datatype.number({ min: 1, max: 20 }),
    },
  ])('should add a todo %j', async (payload) => {
    await request(app.getHttpServer())
      .post('/todo')
      .send(payload)
      .expect(HttpStatus.CREATED)
      .expect(({ body }) => {
        expect(body).toHaveProperty('id');
        expect(body).toHaveProperty('title', payload.title);
        expect(body).toHaveProperty('completed', false);
        expect(body).toHaveProperty('order');
      });
  });

  it('should return validation errors', async () => {
    await request(app.getHttpServer())
      .post('/todo')
      .expect(HttpStatus.BAD_REQUEST)
      .expect(({ body }) => {
        expect(body).toMatchObject({
          error: 'Bad Request',
          message: [
            'title should not be null or undefined',
            'title should not be empty',
            'title must be a string',
          ],
          statusCode: HttpStatus.BAD_REQUEST,
        });
      });
  });

  it('should return all of the todos', async () => {
    await request(app.getHttpServer())
      .get('/todo')
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(Array.isArray(body)).toBe(true);
      });
  });

  it('should get one todo by id', async () => {
    await request(app.getHttpServer())
      .get(`/todo/${todo.id}`)
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(todo).toMatchObject(body);
      });
  });

  it.each([{ completed: true }, { order: 2 }, { title: faker.lorem.words() }])(
    'should update a todo with %j',
    async (payload) => {
      await request(app.getHttpServer())
        .patch(`/todo/${todo.id}`)
        .send(encode(payload))
        .expect(HttpStatus.OK)
        .expect(({ body }) => {
          expect(body.id).toBe(todo.id);
          expect(body.title).toBe(payload.title ?? todo.title);
          expect(body.completed).toBe(payload.completed ?? todo.completed);
          expect(body.order).toBe(payload.order ?? todo.order);
        });
    },
  );

  it('should toggle all of the todos', async () => {
    const payload = { completed: faker.datatype.boolean() };

    await request(app.getHttpServer())
      .patch('/todo')
      .send(payload)
      .redirects(1)
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(Array.isArray(body)).toBe(true);
        expect(
          (body as Todo[]).every(
            (todo) => todo.completed === payload.completed,
          ),
        ).toBe(true);
      });
  });

  it('should delete a todo', async () => {
    await request(app.getHttpServer())
      .delete(`/todo/${todo.id}`)
      .expect(HttpStatus.NO_CONTENT)
      .expect((response) => {
        expect(response.noContent).toBe(true);
        expect(response.text).toBe('');
      });

    await expect(app.get(TodoService).findOne(todo.id)).rejects.toThrow();
  });

  it('should delete all of the todos', async () => {
    await request(app.getHttpServer())
      .delete('/todo')
      .expect(HttpStatus.NO_CONTENT);

    await expect(app.get(TodoService).findAll()).resolves.toHaveLength(0);
  });
});
