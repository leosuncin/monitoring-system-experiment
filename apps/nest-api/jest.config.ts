import { pathsToModuleNameMapper } from 'ts-jest';

import { compilerOptions } from './tsconfig.json';

export default {
  projects: [
    {
      displayName: 'Unit test',
      preset: 'ts-jest',
      testEnvironment: 'node',
      rootDir: 'src',
      moduleDirectories: ['node_modules', __dirname],
      moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths),
    },
    {
      displayName: 'E2E test',
      preset: 'ts-jest',
      testEnvironment: 'node',
      rootDir: 'test',
      moduleDirectories: ['node_modules', __dirname],
      moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths),
    },
  ],
};
