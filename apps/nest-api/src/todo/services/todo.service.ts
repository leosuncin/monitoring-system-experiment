import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import type { Repository } from 'typeorm';

import type { CreateTodo } from '~todo/dto/create-todo.dto';
import type { ToggleTodo } from '~todo/dto/toggle-todo.dto';
import type { UpdateTodo } from '~todo/dto/update-todo.dto';
import { Todo } from '~todo/entities/todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo) private readonly todoRepository: Repository<Todo>,
  ) {}

  create(newTodo: CreateTodo) {
    const todo = this.todoRepository.create(newTodo);

    return this.todoRepository.save(todo);
  }

  findAll() {
    return this.todoRepository.find();
  }

  async findOne(id: Todo['id']) {
    const todo = await this.todoRepository.findOne({ where: { id } });

    if (!todo) {
      throw new NotFoundException(`Not found any todo with id: ${id}`);
    }

    return todo;
  }

  update(todo: Todo, changes: UpdateTodo) {
    this.todoRepository.merge(todo, changes);

    return this.todoRepository.save(todo);
  }

  async updateAll(toggle: ToggleTodo) {
    const result = await this.todoRepository.update({}, toggle);

    return result.affected;
  }

  remove(todo: Todo) {
    return this.todoRepository.remove(todo);
  }

  async removeAll() {
    const result = await this.todoRepository.delete({});

    return result.affected;
  }
}
