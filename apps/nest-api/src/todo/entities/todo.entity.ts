import { Exclude } from 'class-transformer';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Todo {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ type: 'text' })
  title!: string;

  @Column({ type: Boolean, default: false })
  completed!: boolean;

  @Column({ type: 'int', default: 1 })
  order!: number;

  @Exclude()
  @CreateDateColumn()
  readonly createdAt = new Date();

  @Exclude()
  @UpdateDateColumn()
  readonly updatedAt = new Date();
}
