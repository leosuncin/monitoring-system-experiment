import { Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { It, Mock } from 'moq.ts';
import type { FindOptionsWhere, Repository } from 'typeorm';

import { TodoController } from '~todo/controllers/todo.controller';
import type { CreateTodo } from '~todo/dto/create-todo.dto';
import type { ToggleTodo } from '~todo/dto/toggle-todo.dto';
import type { UpdateTodo } from '~todo/dto/update-todo.dto';
import { Todo } from '~todo/entities/todo.entity';
import { TodoService } from '~todo/services/todo.service';

describe('TodoController', () => {
  let controller: TodoController;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        {
          provide: getRepositoryToken(Todo),
          useFactory() {
            const mock = new Mock<Repository<Todo>>();

            mock
              .setup((repository) =>
                repository.create(It.Is<CreateTodo>((dto) => 'title' in dto)),
              )
              // @ts-expect-error mock create
              .returns(new Todo())
              .setup((repository) => repository.save(It.IsAny()))
              .returnsAsync(new Todo())
              .setup((repository) => repository.find())
              .returnsAsync([new Todo()])
              .setup((repository) =>
                repository.update(
                  It.IsAny(),
                  It.Is<ToggleTodo>(
                    (toggle) => typeof toggle.completed === 'boolean',
                  ),
                ),
              )
              .returnsAsync({ generatedMaps: [], raw: [], affected: 1 })
              .setup((repository) => repository.delete(It.IsAny()))
              .returnsAsync({ raw: [], affected: 1 })
              .setup((repository) =>
                repository.findOne(
                  It.Is<{ where: FindOptionsWhere<Todo> }>(
                    (options) => options.where.id === 1,
                  ),
                ),
              )
              .returnsAsync(new Todo())
              .setup((repository) => repository.merge(It.IsAny(), It.IsAny()))
              .callback(({ args }) => Object.assign(args[0], args[1]))
              .setup((repository) =>
                repository.remove(It.Is<Todo>((todo) => todo instanceof Todo)),
              )
              // @ts-expect-error mock remove
              .returnsAsync(new Todo());

            return mock.object();
          },
        },
        TodoService,
      ],
      controllers: [TodoController],
    }).compile();

    controller = module.get(TodoController);
  });

  it('should be an instanceof TodoController', () => {
    expect(controller).toBeInstanceOf(TodoController);
  });

  it('should create a new todo', async () => {
    const newTodo: CreateTodo = {
      title: 'Make a sandwich',
    };
    const todo = await controller.create(newTodo);

    expect(todo).toBeInstanceOf(Todo);
  });

  it('should find all of the todos', async () => {
    const todos = await controller.findAll();

    expect(Array.isArray(todos)).toBe(true);
  });

  it('should toggle all of the todos', async () => {
    await expect(
      controller.toggleAll({ completed: true }),
    ).resolves.toBeDefined();
  });

  it('should remove all of the todos', async () => {
    await expect(controller.removeAll()).resolves.toBeDefined();
  });

  it('should find one todo', async () => {
    const todo = await controller.findOne(1);

    expect(todo).toBeInstanceOf(Todo);
  });

  it('should update one todo', async () => {
    const changes: UpdateTodo = { order: 2 };
    const todo = new Todo();

    await expect(controller.update(todo, changes)).resolves.toBeInstanceOf(
      Todo,
    );
  });

  it('should remove one todo', async () => {
    const todo = new Todo();

    await expect(controller.remove(todo)).resolves.toBeDefined();
  });
});
