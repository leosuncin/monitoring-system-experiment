import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Redirect,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';
import { TodoService } from '~todo/services/todo.service';
import { CreateTodo } from '~todo/dto/create-todo.dto';
import { ToggleTodo } from '~todo/dto/toggle-todo.dto';
import { UpdateTodo } from '~todo/dto/update-todo.dto';
import type { Todo } from '~todo/entities/todo.entity';
import { FindTodoPipe } from '~todo/pipes/find-todo.pipe';

const validationPipe = new ValidationPipe({ whitelist: true, transform: true });

@Controller('todo')
@UseInterceptors(ClassSerializerInterceptor)
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  @Post()
  create(@Body(validationPipe) newTodo: CreateTodo) {
    return this.todoService.create(newTodo);
  }

  @Get()
  findAll() {
    return this.todoService.findAll();
  }

  @Patch()
  @Redirect('/todo', HttpStatus.SEE_OTHER)
  toggleAll(@Body(validationPipe) toggle: ToggleTodo) {
    return this.todoService.updateAll(toggle);
  }

  @Delete()
  @HttpCode(HttpStatus.NO_CONTENT)
  removeAll() {
    return this.todoService.removeAll();
  }

  @Get(':id')
  findOne(@Param('id', ParseIntPipe) id: number) {
    return this.todoService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id', ParseIntPipe, FindTodoPipe) todo: Todo,
    @Body(validationPipe) changes: UpdateTodo,
  ) {
    return this.todoService.update(todo, changes);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  remove(@Param('id', ParseIntPipe, FindTodoPipe) todo: Todo) {
    return this.todoService.remove(todo);
  }
}
