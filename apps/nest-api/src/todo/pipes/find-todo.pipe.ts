import { Injectable, PipeTransform } from '@nestjs/common';

import type { Todo } from '~todo/entities/todo.entity';
import { TodoService } from '~todo/services/todo.service';

@Injectable()
export class FindTodoPipe implements PipeTransform {
  constructor(private readonly todoService: TodoService) {}

  async transform(value: Todo['id']) {
    return this.todoService.findOne(value);
  }
}
