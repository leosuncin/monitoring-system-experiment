import { PickType } from '@nestjs/mapped-types';

import { UpdateTodo } from '~todo/dto/update-todo.dto';

export class ToggleTodo extends PickType(UpdateTodo, ['completed']) {}
