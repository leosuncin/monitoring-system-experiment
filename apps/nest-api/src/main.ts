import type { NestApplicationOptions } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';

import { AppModule } from '~app/app.module';
import type { ConfigObject } from '~app/config/configuration';

export async function bootstrap(options?: NestApplicationOptions) {
  const app = await NestFactory.create(AppModule, options);

  return app;
}

if (require.main === module) {
  void bootstrap().then((app) => {
    const config = app.get<ConfigService<ConfigObject, true>>(ConfigService);

    app.listen(config.get('port'));
  });
}
