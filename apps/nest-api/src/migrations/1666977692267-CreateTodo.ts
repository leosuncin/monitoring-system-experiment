import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateTodo implements MigrationInterface {
  name = 'CreateTodo1666977692267';
  private todoTable = new Table({
    name: 'todo',
    columns: [
      {
        name: 'id',
        type: 'integer',
        isPrimary: true,
        isGenerated: true,
        generationStrategy: 'increment',
      },
      {
        name: 'title',
        type: 'text',
        isNullable: false,
      },
      {
        name: 'completed',
        type: 'boolean',
        isNullable: false,
        default: 0,
      },
      {
        name: 'order',
        type: 'int',
        isNullable: false,
        default: 1,
      },
      {
        name: 'createdAt',
        type: 'datetime',
        isNullable: false,
        default: "datetime('now', 'localtime')",
      },
      {
        name: 'updatedAt',
        type: 'datetime',
        isNullable: false,
        default: "datetime('now', 'localtime')",
      },
    ],
  });

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(this.todoTable);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable(this.todoTable);
  }
}
