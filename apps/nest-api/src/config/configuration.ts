export type ConfigObject = ReturnType<typeof configuration>;

export function configuration() {
  return {
    port: Number.parseInt(process.env.PORT) || 3000,
  };
}
