/// <reference path="../process-env.d.ts" />

import { registerAs } from '@nestjs/config';
import type { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { DataSource, type DataSourceOptions } from 'typeorm';

import { CreateTodo } from '~app/migrations/1666977692267-CreateTodo';
import { Todo } from '~todo/entities/todo.entity';

const baseOptions: DataSourceOptions = {
  type: 'sqlite',
  database: process.env.NODE_ENV !== 'test' ? 'todomvc.db' : ':memory:',
  synchronize: process.env.NODE_ENV === 'test',
};

export const dataSource = registerAs(
  'data-source',
  (): TypeOrmModuleOptions => ({ ...baseOptions, autoLoadEntities: true }),
);

export default new DataSource({
  ...baseOptions,
  entities: [Todo],
  migrations: [CreateTodo],
});
