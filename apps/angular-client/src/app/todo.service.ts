import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { fromFetch } from 'rxjs/fetch';

import { Todo } from './todo.interface';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  private baseURL = environment.baseURL;

  constructor(private readonly http: HttpClient) {}

  createTodo(title: Todo['title']): Observable<Todo> {
    return this.http.post<Todo>(`${this.baseURL}/todo`, { title });
  }

  listTodo(): Observable<Todo[]> {
    return this.http.get<Todo[]>(`${this.baseURL}/todo`);
  }

  updateTodo(
    id: Todo['id'],
    changes: Partial<Pick<Todo, 'completed' | 'order' | 'title'>>,
  ): Observable<Todo> {
    return this.http.patch<Todo>(`${this.baseURL}/todo/${id}`, changes);
  }

  toggleTodos(completed: Todo['completed']): Observable<Todo[]> {
    return fromFetch(`${this.baseURL}/todo`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ completed }),
      redirect: 'follow',
      selector: (response) => response.json(),
    });
  }

  deleteTodo(id: Todo['id']): Observable<unknown> {
    return this.http.delete(`${this.baseURL}/todo/${id}`);
  }
}
