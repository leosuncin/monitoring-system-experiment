import { Component, OnInit } from '@angular/core';

import { pluralize } from './pluralize';
import { Filter } from '../../filter.enum';
import { TodoFacade } from '../../todo.facade';

@Component({
  selector: 'todo-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent implements OnInit {
  activeCount = 0;
  completedCount = 0;
  filter: Filter = Filter.ALL_TODOS;
  readonly Filter = Filter;

  constructor(private readonly todoFacade: TodoFacade) {}

  ngOnInit(): void {
    this.todoFacade.activeCount$.subscribe((activeCount) => {
      this.activeCount = activeCount;
    });
    this.todoFacade.completedCount$.subscribe((completedCount) => {
      this.completedCount = completedCount;
    });
    this.todoFacade.filter$.subscribe((filter) => {
      this.filter = filter;
    });
  }

  get activeTodoWord() {
    return pluralize(this.activeCount, 'item');
  }

  changeFilter(event: Event, filter: Filter) {
    event.preventDefault();
    this.todoFacade.changeFilter(filter);
  }

  clearCompleted() {
    this.todoFacade.clearCompletedTodos();
  }
}
