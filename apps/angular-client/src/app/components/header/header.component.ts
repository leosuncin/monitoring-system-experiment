import { Component } from '@angular/core';

import { TodoFacade } from '../../todo.facade';

@Component({
  selector: 'todo-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent {
  title = '';

  constructor(private readonly todoFacade: TodoFacade) {}

  createTodo(title: string) {
    if (title.trim().length < 1) return;

    this.todoFacade.createTodo(this.title);
    this.title = '';
  }
}
