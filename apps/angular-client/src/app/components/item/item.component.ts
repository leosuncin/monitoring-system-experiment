import { Component, Input, OnInit } from '@angular/core';

import { TodoFacade } from '../../todo.facade';
import { Todo } from '../../todo.interface';

@Component({
  selector: 'todo-item[todo]',
  templateUrl: './item.component.html',
})
export class ItemComponent implements OnInit {
  @Input() todo!: Todo;
  currentTodo!: Todo;

  constructor(private readonly todoFacade: TodoFacade) {}

  ngOnInit(): void {
    this.todoFacade.activeTodo$.subscribe((activeTodo) => {
      this.currentTodo = activeTodo!;
    });
  }

  get editing(): boolean {
    return this.currentTodo?.id === this.todo.id;
  }

  toggle(completed: Todo['completed']) {
    this.todoFacade.updateTodo({ id: this.todo.id, completed });
  }

  edit() {
    this.todoFacade.editTodo(this.todo.id);
  }

  delete() {
    this.todoFacade.deleteTodo(this.todo.id);
  }

  update(title: Todo['title']) {
    if (title.trim().length < 0) return;

    this.todoFacade.updateTodo({ id: this.currentTodo.id, title });
    this.cancelEdit();
  }

  cancelEdit() {
    this.todoFacade.editTodo(0);
  }
}
