import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { TodoFacade } from '../../todo.facade';
import { Todo } from '../../todo.interface';

@Component({
  selector: 'todo-list',
  templateUrl: './list.component.html',
})
export class ListComponent implements OnInit {
  filteredTodos$: Observable<Array<Todo>>;
  activeCount = 0;

  constructor(private readonly todoFacade: TodoFacade) {
    this.filteredTodos$ = todoFacade.filteredTodos$;
  }

  ngOnInit(): void {
    this.todoFacade.activeCount$.subscribe((activeCount) => {
      this.activeCount = activeCount;
    });
  }

  toggleAll(completed: boolean) {
    this.todoFacade.toggleTodos(completed);
  }
}
