import { Injectable } from '@angular/core';
import { EffectFn } from '@ngneat/effects-ng';
import { createStore, select, setProp, withProps } from '@ngneat/elf';
import {
  addEntities,
  deleteEntities,
  getAllEntitiesApply,
  selectActiveEntity,
  selectAllEntities,
  selectAllEntitiesApply,
  selectEntitiesCount,
  selectEntitiesCountByPredicate,
  setActiveId,
  setEntities,
  updateEntities,
  withActiveId,
  withEntities,
} from '@ngneat/elf-entities';
import {
  createRequestDataSource,
  withRequestsStatus,
} from '@ngneat/elf-requests';
import { last, map, mergeMap, Observable, switchMap, tap } from 'rxjs';

import { Filter } from './filter.enum';
import { Todo } from './todo.interface';
import { TodoService } from './todo.service';

const todoStore = createStore(
  { name: 'todo' },
  withEntities<Todo>(),
  withProps<{ filter: Filter }>({ filter: Filter.ALL_TODOS }),
  withActiveId(),
  withRequestsStatus(),
);

const { setSuccess, trackRequestStatus } = createRequestDataSource({
  data$: () => todoStore.pipe(selectAllEntities()),
  requestKey: 'todo',
  dataKey: 'todo',
  store: todoStore,
});

@Injectable({ providedIn: 'root' })
export class TodoFacade extends EffectFn {
  constructor(private readonly todoService: TodoService) {
    super();
  }

  allCount$ = todoStore.pipe(selectEntitiesCount());

  activeCount$ = todoStore.pipe(
    selectEntitiesCountByPredicate((todo) => !todo.completed),
  );

  completedCount$ = todoStore.pipe(
    selectEntitiesCountByPredicate((todo) => todo.completed),
  );

  filter$ = todoStore.pipe(select((state) => state.filter));

  filteredTodos$ = this.filter$.pipe(
    mergeMap((filter) =>
      todoStore.pipe(
        selectAllEntitiesApply({
          filterEntity({ completed }) {
            if (filter === Filter.ALL_TODOS) return true;
            return filter === Filter.COMPLETED_TODOS ? completed : !completed;
          },
        }),
      ),
    ),
  );

  activeTodo$ = todoStore.pipe(selectActiveEntity());

  changeFilter(filter: Filter) {
    todoStore.update(setProp('filter', filter));
  }

  editTodo(id: Todo['id']) {
    todoStore.update(setActiveId(id));
  }

  createTodo = this.createEffectFn((title$: Observable<Todo['title']>) =>
    title$.pipe(
      trackRequestStatus(),
      mergeMap(this.todoService.createTodo.bind(this.todoService)),
      tap((todo) => todoStore.update(addEntities(todo), setSuccess())),
    ),
  );

  listTodo = this.createEffectFn(($: Observable<void>) =>
    $.pipe(
      trackRequestStatus(),
      mergeMap(this.todoService.listTodo.bind(this.todoService)),
      tap((todos) => todoStore.update(setEntities(todos), setSuccess())),
    ),
  );

  updateTodo = this.createEffectFn(
    (
      update$: Observable<
        Pick<Todo, 'id'> & Partial<Pick<Todo, 'completed' | 'order' | 'title'>>
      >,
    ) =>
      update$.pipe(
        trackRequestStatus(),
        mergeMap(({ id, ...changes }) =>
          this.todoService.updateTodo(id, changes),
        ),
        tap((todo) =>
          todoStore.update(updateEntities(todo.id, todo), setSuccess()),
        ),
      ),
  );

  toggleTodos = this.createEffectFn((completed$: Observable<boolean>) =>
    completed$.pipe(
      trackRequestStatus(),
      mergeMap(this.todoService.toggleTodos.bind(this.todoService)),
      tap((todos) => todoStore.update(setEntities(todos), setSuccess())),
    ),
  );

  deleteTodo = this.createEffectFn((id$: Observable<Todo['id']>) =>
    id$.pipe(
      trackRequestStatus(),
      mergeMap((id) => this.todoService.deleteTodo(id).pipe(map(() => id))),
      tap((id) => todoStore.update(deleteEntities(id), setSuccess())),
    ),
  );

  clearCompletedTodos = this.createEffectFn(($: Observable<void>) =>
    $.pipe(
      trackRequestStatus(),
      switchMap(() =>
        todoStore.query(
          getAllEntitiesApply({
            filterEntity(todo) {
              return todo.completed;
            },
          }),
        ),
      ),
      mergeMap(({ id }) => this.todoService.deleteTodo(id).pipe(map(() => id))),
      tap((id) => todoStore.update(deleteEntities(id))),
      last(),
      tap(() => todoStore.update(setSuccess())),
    ),
  );
}
