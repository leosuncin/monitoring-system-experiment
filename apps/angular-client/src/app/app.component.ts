import { Component, OnInit } from '@angular/core';

import { TodoFacade } from './todo.facade';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  allCount = 0;
  activeCount = 0;
  completedCount = 0;

  constructor(private readonly todoFacade: TodoFacade) {}

  ngOnInit(): void {
    this.todoFacade.listTodo();
    this.todoFacade.allCount$.subscribe((allCount) => {
      this.allCount = allCount;
    });
    this.todoFacade.activeCount$.subscribe((activeCount) => {
      this.activeCount = activeCount;
    });
    this.todoFacade.completedCount$.subscribe((completedCount) => {
      this.completedCount = completedCount;
    });
  }

  get showTodoList(): boolean {
    return this.allCount > 0;
  }

  get showFooter(): boolean {
    return this.activeCount > 0 || this.completedCount > 0;
  }
}
