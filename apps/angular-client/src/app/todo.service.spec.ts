import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { Todo } from './todo.interface';
import { TodoService } from './todo.service';

describe('TodoService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: TodoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(TodoService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeInstanceOf(TodoService);
  });

  it('should create a new todo', () => {
    const title = 'Make a salad';
    const todoCreated: Todo = {
      id: 1,
      title,
      completed: false,
      order: 1,
    };

    service.createTodo(title).subscribe((todo) => {
      expect(todo).toEqual(todoCreated);
    });

    const testRequest = httpTestingController.expectOne(
      (request) => request.url.endsWith('/todo') && request.method === 'POST',
    );

    testRequest.flush(todoCreated, { status: 201, statusText: 'Created' });
  });

  it('should find all of the todos', () => {
    const todoList: Array<Todo> = [];

    service.listTodo().subscribe((todos) => {
      expect(Array.isArray(todos)).toBeTrue();
    });

    const testRequest = httpTestingController.expectOne(
      (request) => request.url.endsWith('/todo') && request.method === 'GET',
    );

    testRequest.flush(todoList);
  });

  it('should update one todo', () => {
    const completed = true;
    const todoUpdated: Todo = {
      id: 1,
      title: 'Make a salad',
      completed,
      order: 1,
    };

    service.updateTodo(1, { completed }).subscribe((todo) => {
      expect(todo).toEqual(todoUpdated);
    });

    const testRequest = httpTestingController.expectOne(
      (request) =>
        request.url.endsWith('/todo/1') && request.method === 'PATCH',
    );

    testRequest.flush(todoUpdated);
  });

  it('should remove one todo', () => {
    service.deleteTodo(1).subscribe((response) => {
      expect(response).toBeNull();
    });

    const testRequest = httpTestingController.expectOne(
      (request) =>
        request.url.endsWith('/todo/1') && request.method === 'DELETE',
    );

    testRequest.flush(null, { status: 204, statusText: 'No Content' });
  });
});
