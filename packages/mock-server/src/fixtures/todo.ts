import { faker } from '@faker-js/faker';

import type { CreateTodo, UpdateTodo } from '~/mocks/todo';

export function buildCreateTodo(overrides?: Partial<CreateTodo>): CreateTodo {
  return {
    title: faker.company.catchPhrase(),
    ...overrides,
  };
}

export function buildUpdateTodo(overrides?: Partial<UpdateTodo>): UpdateTodo {
  return {
    ...faker.helpers.arrayElement([
      {
        title: faker.hacker.phrase(),
      },
      {
        completed: faker.datatype.boolean(),
      },
      {
        order: faker.datatype.number({ min: 1 }),
      },
    ]),
    ...overrides,
  };
}
