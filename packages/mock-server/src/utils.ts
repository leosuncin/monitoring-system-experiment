import type { ReasonPhrases, StatusCodes } from 'http-status-codes';

export type ApiError =
  | {
      statusCode: StatusCodes;
      error: ReasonPhrases;
      message: string;
    }
  | {
      statusCode: StatusCodes.UNPROCESSABLE_ENTITY;
      error: ReasonPhrases.UNPROCESSABLE_ENTITY;
      message: string;
      errors: Record<string, string[]>;
    };

export const errorResponseSchema = {
  type: 'object',
  properties: {
    statusCode: {
      type: 'integer',
    },
    error: {
      type: 'string',
    },
    message: {
      type: 'string',
    },
    errors: {
      type: 'object',
    },
  },
  required: ['statusCode', 'error', 'message'],
  additionalProperties: false,
};
