import { factory, primaryKey } from '@mswjs/data';

function* generateAutoIncrement(): Generator<number, number> {
  let id = 1;

  while (true) {
    yield id++;
  }
}

const todoSequence = generateAutoIncrement();
const orderSequence = generateAutoIncrement();

const db = factory({
  todo: {
    id: primaryKey(() => todoSequence.next().value),
    title: String,
    completed: (value?: boolean): boolean => value ?? false,
    order: (value?: number): number => value ?? orderSequence.next().value,
    createdAt: () => new Date(),
    updatedAt: () => new Date(),
  },
});

export default db;
