import { ReasonPhrases, StatusCodes } from 'http-status-codes';
import { rest } from 'msw';

import db from '~/db';
import type { ApiError } from '~/utils';

export type Todo = {
  id: number;
  title: string;
  completed: boolean;
  order: number;
};

export type CreateTodo = Pick<Todo, 'title'> & Partial<Pick<Todo, 'order'>>;

export type UpdateTodo = Partial<Pick<Todo, 'title' | 'completed' | 'order'>>;

type IdParameter = {
  id: string;
};

export const createTodoHandler = rest.post(
  '*/todo',
  async (request, response, context) => {
    const newTodo = await request.json<CreateTodo>();

    const todo = db.todo.create(newTodo);

    return response(context.status(StatusCodes.CREATED), context.json(todo));
  },
);

export const listTodoHandler = rest.get(
  '*/todo',
  (_request, response, context) => {
    const todos = db.todo.findMany({ where: {} });

    return response(context.json(todos));
  },
);

export const toggleTodoHandler = rest.patch(
  '*/todo',
  async (request, response, context) => {
    const { completed } = await request.json<Pick<Todo, 'completed'>>();

    db.todo.updateMany({
      where: {
        completed: {
          notEquals: completed,
        },
      },
      data: { completed },
    });

    return response(
      context.status(StatusCodes.SEE_OTHER),
      context.set('Location', '/todo'),
    );
  },
);

export const removeTodoHandler = rest.delete(
  '*/todo',
  async (_request, response, context) => {
    db.todo.deleteMany({
      where: {},
    });

    return response(
      context.status(StatusCodes.SEE_OTHER),
      context.set('Location', '/todo'),
    );
  },
);

export const updateTodoHandler = rest.patch<any, IdParameter>(
  '*/todo/:id',
  async (request, response, context) => {
    const { completed, title, order } = await request.json<UpdateTodo>();
    const { id } = request.params;
    let todo = db.todo.findFirst({ where: { id: { equals: Number(id) } } });

    if (!todo) {
      return response(
        context.status(StatusCodes.NOT_FOUND),
        context.json<ApiError>({
          statusCode: StatusCodes.NOT_FOUND,
          error: ReasonPhrases.NOT_FOUND,
          message: `Not found any todo with id: ${id}`,
        }),
      );
    }

    todo = db.todo.update({
      data: {
        completed: completed ?? todo.completed,
        order: order ?? todo.order,
        title: title ?? todo.title,
        updatedAt: new Date(),
      },
      where: { id: { equals: Number(id) } },
    });

    return response(context.json(todo));
  },
);

export const deleteTodoHandler = rest.delete<never, IdParameter>(
  '*/todo/:id',
  (request, response, context) => {
    const { id } = request.params;

    try {
      db.todo.delete({
        where: { id: { equals: Number(id) } },
        strict: true,
      });

      return response(context.status(StatusCodes.NO_CONTENT));
    } catch {
      return response(
        context.status(StatusCodes.NOT_FOUND),
        context.json<ApiError>({
          statusCode: StatusCodes.NOT_FOUND,
          error: ReasonPhrases.NOT_FOUND,
          message: `Not found any todo with id: ${id}`,
        }),
      );
    }
  },
);
