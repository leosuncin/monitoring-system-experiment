import * as http from 'node:http';
import { createServer } from '@mswjs/http-middleware';
import test from 'ava';
import { ReasonPhrases, StatusCodes } from 'http-status-codes';
import { e2e, request, spec } from 'pactum';
import listen from 'test-listen';

import {
  buildCreateTodo,
  buildUpdateTodo,
  createTodoHandler,
  listTodoHandler,
  toggleTodoHandler,
  removeTodoHandler,
  updateTodoHandler,
  deleteTodoHandler,
} from '~/index';
import { errorResponseSchema } from '~/utils';

const testCase = e2e('Todo CRUD');
const httpServer = http.createServer(
  createServer(
    createTodoHandler,
    listTodoHandler,
    toggleTodoHandler,
    removeTodoHandler,
    updateTodoHandler,
    deleteTodoHandler,
  ),
);

const notFoundError = test.macro(
  async (t, path: `${'DELETE' | 'PATCH'} /todo/${number}`) => {
    const [method, url] = path.split(' ');

    await spec()
      .withPath(url!)
      .withMethod(method!)
      .withBody({ title: 'error', completed: true })
      .expectStatus(StatusCodes.NOT_FOUND)
      .expectJsonSchema(errorResponseSchema)
      .expectJsonMatch({
        statusCode: StatusCodes.NOT_FOUND,
        error: ReasonPhrases.NOT_FOUND,
      })
      .toss();

    t.pass();
  },
);

test.before(async (t) => {
  const url = await listen(httpServer);

  request.setBaseUrl(url);

  t.pass();
});

test.after(async () => {
  await testCase.cleanup();
  httpServer.close();
});

test.serial('add a todo', async (t) => {
  const data = buildCreateTodo();

  await testCase
    .step('Add todo')
    .spec()
    .post('/todo')
    .withJson(data)
    .expectStatus(StatusCodes.CREATED)
    .expectJsonLike({
      id: 'typeof $V === "number"',
      title: data.title,
      completed: false,
      order: 'typeof $V === "number"',
    })
    .stores('todo', '.')
    .clean()
    .delete('/todo/{todoId}')
    .withPathParams('todoId', '$S{todo.id}')
    .toss();

  t.pass();
});

test.serial('list return all of the todos', async (t) => {
  await testCase
    .step('List all the todo')
    .spec()
    .get('/todo')
    .expectStatus(StatusCodes.OK)
    .expectJsonLike('.', '$V.length >= 1')
    .toss();

  t.pass();
});

test.serial('update a todo', async (t) => {
  const data = buildUpdateTodo();

  await testCase
    .step('Update a todo')
    .spec()
    .patch('/todo/{todoId}')
    .withPathParams('todoId', '$S{todo.id}')
    .withJson(data)
    .expectStatus(StatusCodes.OK)
    .expectJsonLike({
      id: '$S{todo.id}',
      title: data.title ?? 'typeof $V === "string"',
      completed: data.completed ?? 'typeof $V === "boolean"',
      order: data.order ?? 'typeof $V === "number"',
    })
    .toss();

  t.pass();
});

test(
  'fail to update when todo not exist',
  notFoundError,
  `PATCH /todo/${Date.now()}`,
);

test('toggle all of the todos', async (t) => {
  const payload = { completed: Math.random() > 0.5 };

  await testCase
    .step('Toggle all the todos')
    .spec()
    .patch('/todo')
    .withJson(payload)
    .expectStatus(StatusCodes.SEE_OTHER)
    .expectHeader('location', '/todo')
    .toss();

  t.pass();
});

test('delete all of the todos', async (t) => {
  await spec().delete('/todo').expectStatus(StatusCodes.SEE_OTHER).toss();

  t.pass();
});
