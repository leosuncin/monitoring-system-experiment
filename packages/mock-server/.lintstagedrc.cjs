const baseConfig = require('../../.lintstagedrc.json');

module.exports = {
  ...baseConfig,
  '*.{js,cjs,mjs,ts}': ['xo --cache --fix'],
};
